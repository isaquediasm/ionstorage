angular.module('ionStorage', [])

.service('storage', ['$window',
    function($window) {

        var storage = window.localStorage;
        var it = this;

        // Quando o localStorage sofre alterações, propagamos-as utilizando o $rootScope.$apply() 
        angular.element($window).on('storage', function(event) {
            if (event.key === 'animais') {
                $rootScope.$apply();
            }
        });

        // Salva itens no localStorage
        it.post = function(key, value) {

            try {
                // Tenta serializar para JSON o valor que foi passado através do parâmetro
                value = JSON.stringify(value);
            } catch (e) {}

            // Salva no localStorage
            storage.setItem(key, value);
        }

        // Retorna um item do localStorage
        it.get = function(key) {
            return JSON.parse(storage.getItem(key));
        }

        // Retorna uma determinada posição num vetor.
        it.getByIndex = function(key, index) {
            var items = it.get(key);
            return items[index];
        }

        // Salvar um vetor no localStorage. Se este já existir, apenas insere um novo valor neste array.
        it.save = function(key, value) {

            // Checa se há algum item salvo no localStorage
            if (it.get(key)) {

                var valueToInsert = value;

                // Extrai o json que está no localStorage
                value = it.get(key);
                value.push(valueToInsert);

            } else {

                // Se não houver este item no localStorage, inserimos o JSON num vetor.
                value = [value];

            }

            // Salva no localStorage
            it.post(key, value);

        }

        // Atuliza uma determinada posição num vetor.
        it.update = function(key, index, value) {
            var item = it.get(key, index);
            item[index] = value;
            it.post(key, item);
        }

        it.clear = function() {
            storage.clear();
        }

        it.remove = function(key) {
            storage.removeItem(key);
        }

    }
])
